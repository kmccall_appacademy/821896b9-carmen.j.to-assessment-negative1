# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  numbers_needed = []
  (nums.min...nums.max).each do |n|
    if !nums.include?(n)
      numbers_needed << n
    end
  end
  numbers_needed
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  sum = []
  num_arr = binary.split('').reverse
  num_arr.each_with_index do |n, idx|
    sum << n.to_i * (2**idx)
  end
  sum.reduce(:+)
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    result = {}
    self.each do |k, v|
      if prc.call(k, v) == true
        result[k] = v
      end
    end
    result
  end
end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    result = {}
    # NO PROC
    if !block_given?
      self.each do |k, v|
        if hash.has_key?(k)
          result[k] = hash[k]
        else
          result[k] = v
        end
      end
      hash.each do |k, v|
        if !self.has_key?(k)
          result[k] = hash[k]
        end
      end
    # WITH PROC
    else
      self.keys.each do |key|
        if self[key] && hash[key]
          result[key] = prc.call(key, self[key], hash[key])
        else
          result[key] = self[key]
        end
      end

      hash.keys.each do |key|
        unless self[key] && hash[key]
          result[key] = hash[key]
        end
      end
    end
    result
  end
end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  lucas_seq = [2, 1]
  idx = 0
  while idx < (n.abs)-1
    lucas_seq << lucas_seq[idx..idx+1].inject(:+)
    idx += 1
  end
  if n >= 0
    lucas_seq[n]
  elsif n < 0
    if n.abs.odd?
      lucas_seq[n.abs] * -1
    else
      lucas_seq[n.abs]
    end
  end
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  longest_pal = nil
  i = 0
  while i < string.length
    if string.length.odd?
      if longest_pal.nil? || find_the_longest_odd_pal(string, i).length > longest_pal.length
        longest_pal = find_the_longest_odd_pal(string, i)
      end
    else
      if longest_pal.nil? || find_the_longest_even_pal(string, i).length > longest_pal.length
        longest_pal = find_the_longest_even_pal(string, i)
      end
    end
    i += 1
  end
  return false if longest_pal.length <= 2
  longest_pal.length
end

def find_the_longest_odd_pal(input, idx)
  idx_offset = 0
  while input[idx + idx_offset] == input[idx - idx_offset] &&
    (idx + idx_offset < input.length && idx - idx_offset >= 0)
    idx_offset += 1
  end
  idx_offset -= 1
  input[(idx - idx_offset)..(idx + idx_offset)]
end

def find_the_longest_even_pal(input, idx)
   idx_offset = 0
   while input[idx - idx_offset] == input[idx + 1 + idx_offset] &&
    idx - idx_offset >= 0 && idx + 1 + idx_offset < input.length
    idx_offset += 1
  end
    idx_offset -= 1
    input[(idx - idx_offset)..(idx + idx_offset + 1)]
end
